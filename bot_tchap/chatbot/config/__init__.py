from dotenv import load_dotenv
import os

#Chemin du repertoire parent
script_directory = os.path.dirname(os.path.abspath(__file__))
parent_directory = os.path.dirname(script_directory)
env_directory = os.path.dirname(parent_directory)

# Charge les variables d'environnement
load_dotenv(dotenv_path=env_directory + '/.secrets.env')
load_dotenv(dotenv_path=env_directory + '/.common.env')

# Chargement des variables d'environnement
TCHAP_URL = os.getenv('TCHAP_URL')
TCHAP_USER_ID = os.getenv('TCHAP_USER_ID')
TCHAP_PASSWORD = os.getenv('TCHAP_PASSWORD')
TCHAP_ROOM_ID = os.getenv('TCHAP_ROOM_ID')

