import sys
import os
import simplematrixbotlib as botlib
import requests
import logging
import asyncio
import datetime
# Désactiver les logs de débogage pour les modules spécifiques
#logging.getLogger("peewee").setLevel(logging.INFO)
#logging.getLogger("nio").setLevel(logging.INFO)

# Ou ajuster le niveau global si vous préférez moins de détails dans les logs
logging.basicConfig(level=logging.INFO)



#chemin du répertoire parent
script_directory = os.path.dirname(os.path.abspath(__file__))
parent_directory = os.path.dirname(script_directory)
sys.path.append(parent_directory)

from config import (TCHAP_URL,
                    TCHAP_USER_ID,
                    TCHAP_PASSWORD,
                    TCHAP_ROOM_ID)



print(TCHAP_URL
      ,TCHAP_USER_ID
      ,TCHAP_PASSWORD
      ,TCHAP_ROOM_ID)

creds = botlib.Creds(
    homeserver=TCHAP_URL,
    username=TCHAP_USER_ID,
    password=TCHAP_PASSWORD,
)

bot = botlib.Bot(creds)

room_id = TCHAP_ROOM_ID
PREFIX = '!'

bot_start_time = datetime.datetime.now(datetime.timezone.utc)
print(bot_start_time)

@bot.listener.on_message_event
async def echo(room: str, message) -> None:

    #display all the types of message to debug
    print(f"Message from {message.sender}: {message.body}")
    print(vars(message))
    
    # Convertit l'horodatage du message en objet datetime
    message_time = datetime.datetime.fromtimestamp(message.server_timestamp / 1000, tz=datetime.timezone.utc)
    print(message_time)

    # Vérifie si le message a été envoyé après le démarrage du bot
    print(message_time > bot_start_time)
    if message_time > bot_start_time:
        print(f"Message from {message.sender}: {message.body}")
        print(room.room_id)
        if room.room_id == room_id:
            print("good room")
            match = botlib.MessageMatch(room, message, bot, PREFIX)

            """if match.is_not_from_this_bot():
                print("not from this bot")"""

            if match.prefix() and match.command("echo"):
                await bot.api.send_text_message(room.room_id," ".join(arg for arg in match.args()))
            
            if match.prefix() and match.command("help"):
                await bot.api.send_text_message(room.room_id,"Available commands: !echo <message>, !help")
            
            if match.prefix() and match.command("bot"):
                #Enregistre la reponse pour la requete suivante sur le port 8000 de host.internal vers le endpoint /confluence-rag-agent en python avec la librairie requests en mode asynchrone (avec await)
                question = " ".join(arg for arg in match.args())
                try: 
                    response = requests.post("http://model_api:8000/confluence-rag-agent", json={"text": question})
                except:
                    try:
                        response = requests.post("http://localhost:8000/confluence-rag-agent", json={"text": question})
                    except Exception as e:
                        await bot.api.send_text_message(room.room_id, f"Error: {e}")
                        return
                print(response.json())
                await bot.api.send_text_message(room.room_id, response.json())



def main() -> None:
    loop = asyncio.get_event_loop()
    if loop.is_running():
        # Planifie bot.run() pour s'exécuter sur la boucle d'événement existante
        loop.create_task(bot.run())
    else:
        # Exécute bot.run() sur une nouvelle boucle d'événement
        loop.run_until_complete(bot.run())

if __name__ == "__main__":
    main()
