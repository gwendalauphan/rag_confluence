from pydantic import BaseModel


class ConfluenceQueryInput(BaseModel):
    text: str


class ConfluenceQueryOutput(BaseModel):
    input: str
    output: str
    #intermediate_steps: list[str]