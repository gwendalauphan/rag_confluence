from fastapi import FastAPI
from chains.confluence_rag_chain import infos_chain
from models.confluence_rag_query import ConfluenceQueryInput, ConfluenceQueryOutput
from utils.async_utils import async_retry

app = FastAPI(
    title="Confluence Chatbot",
    description="Endpoints for confluence RAG chatbot",
)


@async_retry(max_retries=10, delay=1)
async def invoke_agent_with_retry(query: str):
    """
    Retry the agent if a tool fails to run. This can help when there
    are intermittent connection issues to external APIs.
    """

    return infos_chain.invoke(query)


@app.get("/")
async def get_status():
    return {"status": "running"}

@app.post("/confluence-rag-agent")
async def query_confluence_agent(query: ConfluenceQueryInput,): # -> ConfluenceQueryOutput:
    query_response = await invoke_agent_with_retry(query.text)
    return query_response