import os
import sys

#chemin du répertoire parent
script_directory = os.path.dirname(os.path.abspath(__file__))
parent_directory = os.path.dirname(script_directory)
project_directory = os.path.dirname(parent_directory)

sys.path.append(parent_directory)
sys.path.append(project_directory)

from config import CONFLUENCE_AGENT_MODEL, PERSIST_DIRECTORY

from confluence_etl.embedding import embeddings
from confluence_etl.database import DataLoader

from langchain.prompts import (
    ChatPromptTemplate,
    HumanMessagePromptTemplate,
    PromptTemplate,
    SystemMessagePromptTemplate,
)
from langchain_core.runnables import RunnablePassthrough
from langchain_core.output_parsers import StrOutputParser

from langchain_openai import ChatOpenAI

#Vérifier si le dossier PERSIST_DIRECTORY existe et est différend de vide

if not os.path.exists(PERSIST_DIRECTORY):
    os.makedirs(PERSIST_DIRECTORY)
#Vérifier si le dossier PERSIST_DIRECTORY est différent de vide
if not os.listdir(PERSIST_DIRECTORY):
    #Si le dossier est vide, on charge les données
    db = DataLoader().set_db(embeddings)
else:
    db = DataLoader().get_db(embeddings)


infos_template_str_fr = """Vous êtes un assistant pour les tâches de questions-réponses. 
Utilisez les morceaux de contexte récupérés suivants pour répondre à la question. 
Si vous ne connaissez pas la réponse, dites simplement que vous ne savez pas. 
Utilisez au maximum trois phrases et gardez la réponse concise.

{context}
"""

infos_template_str_en = """You are an assistant for question-answering tasks.
Use the following retrieved context chunks to answer the question.
If you don't know the answer, just say you don't know.
Use up to three sentences and keep the answer concise.

{context}
"""

def format_docs(docs):
    return "\n\n".join(doc.page_content for doc in docs)

infos_system_prompt = SystemMessagePromptTemplate(
    prompt=PromptTemplate(
        input_variables=["context"],
        template=infos_template_str_fr,
    )
)

infos_human_prompt = HumanMessagePromptTemplate(
    prompt=PromptTemplate(
        input_variables=["question"],
        template="{question}",
    )
)

messages = [infos_system_prompt, infos_human_prompt]


infos_prompt = ChatPromptTemplate(
    input_variables=["context", "question"],
    messages=messages,
)

infos_chain = (
    {"context": db.as_retriever(k=3)| format_docs, "question": RunnablePassthrough()}
    | infos_prompt
    | ChatOpenAI(model=CONFLUENCE_AGENT_MODEL, temperature=0)
    | StrOutputParser()
)

