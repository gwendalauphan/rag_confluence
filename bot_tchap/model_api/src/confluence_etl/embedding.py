import os
from langchain_community.embeddings import HuggingFaceEmbeddings

cache_dir_path_embedding = os.getenv('PERSIST_DIRECTORY')


model_name_embedding = 'dangvantuan/sentence-camembert-base'
model_kwargs = {"trust_remote_code": True} #"device": "cpu",
encode_kwargs = {
    "normalize_embeddings": True,
}

embeddings = HuggingFaceEmbeddings(
    cache_folder=cache_dir_path_embedding,
    model_name=model_name_embedding,
    model_kwargs=model_kwargs,
    encode_kwargs=encode_kwargs,
)

