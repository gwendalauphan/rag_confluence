from dotenv import load_dotenv
import os

#Chemin du repertoire parent
script_directory = os.path.dirname(os.path.abspath(__file__))
parent_directory = os.path.dirname(script_directory)
env_directory = os.path.dirname(parent_directory)



# Charge les variables d'environnement
load_dotenv(dotenv_path=env_directory + '/.secrets.env')
load_dotenv(dotenv_path=env_directory + '/.common.env')

# Chargement des variables d'environnement
OPENAI_API_KEY = os.getenv('OPENAI_API_KEY')
CONFLUENCE_PRIVATE_USER_API_KEY = os.getenv('CONFLUENCE_PRIVATE_USER_API_KEY')
CONFLUENCE_SPACENAME_KEY = os.getenv('CONFLUENCE_SPACENAME_KEY')
CONFLUENCE_WORKSPACE_URL = os.getenv('CONFLUENCE_WORKSPACE_URL')
CONFLUENCE_EMAIL_ADRESS = os.getenv('CONFLUENCE_EMAIL_ADRESS')


cache_dir_path_vector_store = parent_directory
PERSIST_DIRECTORY = cache_dir_path_vector_store +'/db/chroma/'
#Create the directory if it does not exist
if not os.path.exists(PERSIST_DIRECTORY):
    os.makedirs(PERSIST_DIRECTORY)
#Exporter la variable d'environnement
os.environ['PERSIST_DIRECTORY'] = PERSIST_DIRECTORY


CONFLUENCE_AGENT_MODEL = "gpt-3.5-turbo-0125"